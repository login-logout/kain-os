#!/bin/bash
 
# Get environment variables of the project
cd $(dirname $(readlink -f "$0"))
source variables.sh

googlePreferences="$home/.config/google-chrome/Default/Preferences"

# Run this script in display 0 - the monitor
export DISPLAY=:0

# enable ethernet
{
iname=$(ls /sys/class/net | grep -i enp)
sudo ip link set $iname up
sudo dhclient $iname
} &


# For audio
amixer -c 0 set Master playback 0% mute
amixer -c 0 set Master playback 100% unmute


# Fix crashbubble in chrome
sudo chattr +i $googlePreferences

# Run Server
{
cd ..
while true; do
	sudo node index
done
} &

sleep 2

# Run Chrome
{
while true; do
	# If Chrome crashes (usually due to rebooting), clear the crash flag so we don't have the annoying warning bar
	# sudo sed -i 's/\"exited_cleanly\":false/\"exited_cleanly\":true/' $googlePreferences
	# sudo sed -i 's/\"exit_type\":\"Crashed\"/\"exit_type\":\"Normal\"/' $googlePreferences
	sudo google-chrome-stable --test-type --no-sandbox --ignore-certificate-errors --restore-last-session --disable-infobars --kiosk --no-first-run  https://localhost
done
} &

# etc/cloud/cloud.cfg.d/99-disable-network-config.cfg
network:
    optional: true