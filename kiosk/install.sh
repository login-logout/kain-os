#!/bin/bash

# this tutorial helped me a lot https://obrienlabs.net/setup-kiosk-ubuntu-chromium/

user=${1:-$USER}
home=${2:-$HOME}
repo=https://bitbucket.org/login-logout/kain-os.git
cloneDir=$home
workingDir=$cloneDir/kain-os
kioskScript=$workingDir/kiosk/run.sh
lightdmConf=/etc/lightdm/lightdm.conf
lightdmCustomConf=/etc/lightdm/lightdm.conf.d/50-autologin.conf
autostartScript=$home/.xprofile
variables=$workingDir/kiosk/variables.sh
i3Src=$workingDir/kiosk/i3.conf
i3Dest=$home/.config/i3/config


# Check for SUDO
if [ $EUID != 0 ]; then
    sudo "$0" "$user" "$home"
    exit $?
fi

# Setup
clear
echo
echo
echo "=== Login Logout: Kain Pull OS ==="
echo "(for user $user)"
echo
echo

# ------------------------------------ Installing Packages/Dependencies

echo "Installing Dependencies..."

# add chrome to the list
echo "> Getting Chromes' repository..."
wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
echo 'deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main' | sudo tee /etc/apt/sources.list.d/google-chrome.list > /dev/null

echo
echo
echo
echo "> Update Package List"
sudo apt -qq update

echo
echo
echo
echo "> Upgrade"
sudo apt -qq -y upgrade

echo
echo
echo

echo "> Disabling unneeded services"

sudo systemctl mask snapd.service snapd.seeded.service snapd.socket

sudo apt-get purge -y network-manager

echo "> Installing needed packages"

sudo apt install -y i3 lightdm google-chrome-stable pulseaudio alsa-utils git npm nodejs openssh-server
sudo apt autoremove -y

clear

# ------------------------------------ Installing the actual Project
echo "Installing the OS..."
sudo mkdir -p $workingDir
cd $cloneDir
echo " > Cloning project..."
sudo git clone -q $repo
cd $workingDir
echo " > Installing all node-dependencies"
sudo npm install --silent
echo " > Building the project"
sudo npm run --silent build > /dev/null


# ----------------------------------- Startup Script and Desktop Environment
echo "Configuration and Autostart..."

# Autologin
echo "[SeatDefaults]
autologin-user=$user
autologin-user-timeout=0
user-session=i3
greeter-session=unity-greeter
" | sudo tee $lightdmConf > /dev/null

echo "[SeatDefaults]
autologin-user=$user
" | sudo tee $lightdmCustomConf > /dev/null

# Autostart
echo "#!/bin/bash
$kioskScript &
" | sudo tee $autostartScript > /dev/null

# Variables for other scripts
echo "#!/bin/bash
export user=$user
export home=$home
export workingDir="$cloneDir/kain-os"
export kioskScript="$workingDir/kiosk/run.sh"
" | sudo tee $variables > /dev/null

# I3 Config
sudo mkdir -p "$(dirname $i3Dest)"
sudo cp $i3Src $i3Dest

# Cloud init config to improve boot speed
sudo cp cloud-init.yaml /etc/netplan/50-cloud-init.yaml
sudo netplan --debug generate
sudo netplan apply

# Chmods
sudo chmod +x $autostartScript
sudo chmod +x $kioskScript

# Disable password prompt for the user (scripts need root privileges)
echo "$user ALL=(ALL) NOPASSWD:ALL" | sudo EDITOR='tee -a' visudo > /dev/null

# To mount an usb-stick
sudo mkdir -p /media/usb
sudo chown -R $user:$user /media/usb

# Simple permissions
sudo chown -R $user:$user $home


# ==== Finished
echo
echo "FINISHED!"
echo "Booting the system..."
sudo systemctl restart lightdm