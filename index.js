// imports
const express = require('express');
const cookieSession = require('cookie-session');
const bodyParser = require('body-parser');
const passport = require('passport');
const passportLocal = require('passport-local');
const multer = require('multer');
const fs = require('fs');
const path = require('path');
const child_process = require('child_process');
const https = require('https');


// initialize
const app = express();
const LocalStrategy = passportLocal.Strategy;
const publicRoot = path.join( __dirname, './dist' );
const projectRoot = `${publicRoot}/assets/project/`
const projectConf = `${projectRoot}/config.json`;
const usersConf = path.join( __dirname, './users.json' );
const sslKey = fs.readFileSync( path.join(__dirname, 'sslcert/server.key'), 'utf8' );
const sslCert = fs.readFileSync(path.join(__dirname, 'sslcert/server.crt'), 'utf8');

const httpPort = 80;
const httpsPort = 443;

const upload = multer({
    storage: multer.diskStorage({
        destination(request, file, callback) {
            callback( null, projectRoot );
        },
        filename(request, file, callback) {
            callback( null, file.originalname );
        }
    })
});

const usbUpload = multer({
    storage: multer.diskStorage({
        async destination(request, file, callback) {
            const path = await getUSBPath();
            if (path) {
                callback( null, path );
            } else {
                callback( 'failed to get path', path );
            }
        },
        filename(request, file, callback) {
            callback( null, file.originalname );
        }
    })
});

// connecting modules with the app
app.use( bodyParser.json());

app.use( cookieSession({
    name: 'token',
    keys: ['auth'],
    maxAge: 1000 * 60 * 60 * 24 * 5 // 10 days
}));

app.use( passport.initialize() );

app.use( passport.session() );

app.use( express.static(publicRoot) );

// configure passport
passport.use(
    new LocalStrategy({
        usernameField: 'username',
        passwordField: 'password'
    }, async (username, password, done) => {
        const users = await loadUsers();
        const user = users.find( user => user.username === username && user.password === password );
        if (user) {
            done( null, user );
        } else {
            done( null, false, { status: 'unsuccessfull', error: 'Incorrect username or password' })
        }
    })
);

passport.serializeUser( (user, done) => {
    done( null, user.id );
});

passport.deserializeUser( async (id, done) => {
    const users = await loadUsers();
    done( null, users.find( user => user.id === id ) );
});


// api routes
app.post("/api/login", (request, response, next) => {
    passport.authenticate("local", (error, user, info) => {
        if (error) {
            return next(error);
        }
        if (!user) {
            return response.status(400).send({ status: 'unsuccessfull' });
        }
        request.login(user, error => {
            response.send({ status: 'successfull' });
        });
    })(request, response, next);
});

app.post("/api/logout", (request, response) => {
    request.logout();
    return response.send({ status: 'successfull' });
});

app.get("/api/loggedin", authMiddleware, (request, response) => {
    response.send({ status: 'successfull', username: request.user.username });
});

app.get("/api/data", authMiddleware, async (request, response) => {
    const data = await loadData();
    response.send({ status: 'successfull', data });
});

app.post('/api/data/files', authMiddleware, upload.fields([ 'filesystem', 'avatar', 'wallpaper', 'ping' ].map( name => ({ name, maxCount: 1 }))), async (request, response, next) => {
    console.log('updated ' + Object.keys(request.files).join(',')); 
    response.send({ status: 'successfull' });
});

// usb login
app.get('/api/usb/device/there', async (request, response, next) => {
    if (await hasUSB()) {
        response.send({ status: true });
    } else {
        response.send({ status: false });
    }
});

// usb upload
app.get('/api/usb/device', async (request, response, next) => {
    if (await getUSBPath()) {
        response.send({ status: 'successfull' });
    } else {
        response.send({ status: 'unsuccessfull' });
    }
});

app.post('/api/usb/upload', usbUpload.single('file'), async (request, response, next) => {
    const usb = await getUSBPath();
    if (!usb) {
        response.send({ status: 'unsuccessfull' });
    } else {
        syncUSB( usb, response );
    }
});

// format = remove the downloaded file
app.post('/api/usb/format', async (request, response, next) => {
    const usb = await getUSBPath();
    if (!usb) {
        response.send({ status: 'unsuccessfull' });
    } else {
        const config = await loadData();
        const filename = config.forceDownloadFile.slice(config.forceDownloadFile.lastIndexOf('/') || 0);
        try {
            fs.unlinkSync(path.join(usb, filename));
            console.log('removed the downloaded file');
        } catch(e) {
            console.log('couldn\'t remove the downloaded file');
        }
        
        syncUSB( usb, response );
    }
});

app.post("/api/data/config", authMiddleware, async (request, response) => {
    const newConfig = request.body;
    const config = await loadData();
    let updatedConfig = { ...config, ...newConfig };
    await saveData( projectConf, updatedConfig );
    console.log('updated config');
    return response.send({ status: 'successfull' });
});

app.post("/api/data/login", authMiddleware, async (request, response) => {
    try {
        const { username, password } = request.body;
        saveData( usersConf, {
            users: [{
                id: 1,
                username, password
            }]
        });
        console.log('updated login');
        return response.send({ status: 'successfull' });
    } catch(e) {
        console.error(e);
        return response.send({ status: 'unsuccessfull' });
    }
});


// vue route
app.get("*", (request,response, next) => {
    response.sendFile( 'index.html', { root: publicRoot } );
});




// run the app
const httpRedirect = express();
httpRedirect.get( '*', (request, response) => {
    response.redirect(`https://${request.headers.host}:${httpsPort}${request.url}`);
});

const httpsServer = https.createServer( { cert: sslCert, key: sslKey }, app );

httpRedirect.listen( httpPort, () => {
    console.log('http server started');
})

httpsServer.listen( httpsPort, () => {
    console.log('https server started');
});






// functions
function authMiddleware( request, response, next ) {
    if (!request.isAuthenticated()) {
        response.status(401).send({ status: 'unsuccessfull' });
    } else {
        return next();
    }
}

async function loadData( file = projectConf ) {
    return new Promise( (resolve, reject) => fs.readFile( file, (error, data) => {
        if (error) {
            reject(error);
        } else {
            const json = JSON.parse(data);
            resolve(json);
        }
    }));
}

async function saveData( file = projectConf, data ) {
    return new Promise( (resolve, reject) => fs.writeFile( file, JSON.stringify(data), error => {
        if (error) {
            reject(error);
        } else {
            resolve();
        }
    }));
}

async function loadUsers() {
    return (await loadData(usersConf)).users;
}

async function hasUSB() {
    return new Promise( (resolve, reject) => {
        child_process.exec(`lsblk -J`, (error, stdout, stderr) => {
            if (error || stderr) {
                reject({ error, stderr });
            }
            try {
                const drives = JSON.parse(stdout).blockdevices;
                const usbs = drives.filter( drive => drive.name.slice(0, 2) === 'sd' )
                                   .filter( drive => drive.rm === '1' && drive.ro === '0' );
                
                if (usbs.length) {
                    resolve(true)
                } else {
                    resolve(false);
                }
            } catch(e) {
                reject(e);
            }
        });
    });
}

async function getUSBPath() {
    return new Promise( (resolve, reject) => {
        child_process.exec(`lsblk -J`, (error, stdout, stderr) => {
            if (error || stderr) {
                reject({ error, stderr });
            }
            try {
                const drives = JSON.parse(stdout).blockdevices;
                const usbs = drives.filter( drive => drive.name.slice(0, 2) === 'sd' )
                                   .filter( drive => drive.rm === '1' && drive.ro === '0' );
                
                if (usbs.length) {
                    const usb = usbs[0];
                    const mountpoint = usb.mountpoint || (usb.children && usb.children[0].mountpoint);

                    if (!mountpoint) {
                        // mount it
                        const child = (usb.children && usb.children[0].name) || usb.name;
                        child_process.exec(`sudo mount /dev/${child} /media/usb`, (error, stdout, stderr) => {
                            if (!error && !stderr) {
                                resolve('/media/usb');
                            } else {
                                reject(error);
                            }
                        });

                    } else {
                        resolve(mountpoint);
                    }
                } else {
                    resolve(null);
                }
            } catch(e) {
                reject(e);
            }
        });
    });
}

async function syncUSB( path, response ) {
    return new Promise( (resolve, reject) => {
        child_process.exec(`sync`, (error, stdout, stderr) => {
            if (!error && !stdout && !stderr) {
                console.log('synced the usb')
                response.send({ status: 'successfull' });
                resolve();
            } else {
                console.log('failed to sync the usb', { error, stdout, stderr })
                response.send({ status: 'unsuccessfull' });
                reject({ error, stdout, stderr });
            }
        });
    });
}
