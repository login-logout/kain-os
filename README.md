# KAIN-OS

## Setup of Ubuntu-Server-Kiosk
```
wget -O - https://bitbucket.org/login-logout/kain-os/raw/HEAD/kiosk/install.sh | sh
```


---
## Setup of Node Server


### Project setup
```
npm install
```

### Building the project
```
npm run build
```

### Running it
```
node index
```
