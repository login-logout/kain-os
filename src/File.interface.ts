import Directory from './Directory.class';

export default class File {
    constructor( public name: string,
                 public content: Blob,
                 public parent: Directory ) {
    }

    getFullPath() {
        return this.parent.getFullPath() + '/' + this.name;
    }
}