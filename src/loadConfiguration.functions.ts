import * as JSZip from 'jszip';
import Directory from './Directory.class';
import File from './File.interface';
import { CorruptedDirectory } from './CorruptedDirectory.class';

const corruptedDirectory = loadCorruptedDirectory();
const configuration = loadConfiguration();

export async function getCorruptedDirectory() {
    return corruptedDirectory;
}

export async function getConfiguration() {
    return configuration;
}

export async function loadCorruptedDirectory( url?: string, name?: string ): Promise<CorruptedDirectory> {
    const config = await loadConfiguration();
    const directory = await loadDirectory( url, name );
    return new CorruptedDirectory( directory.name, config.corruptedSubdirectoryName, null, directory.directories, directory.files );
}

export async function loadDirectory( url?: string, name = 'Dokumente' ): Promise<Directory> {
    const directory: Directory = new Directory(name);
    const zip = await loadZip(url);
    // bring it to my format
    const files: JSZip.JSZipObject[] = [];
    zip.forEach( (filename: string, file: JSZip.JSZipObject) => files.push(file) );
    await Promise.all(files.map( async file => {
        let path = file.name;
        let filename = null;
        if (file.dir) {
            path = path.slice(0, -1);
        } else {
            const lastSlash = path.lastIndexOf('/');
            filename = path.slice( lastSlash + 1 );
            path = path.slice( 0, lastSlash );
        }
        const context = directory.createDirectory(path);
        if (!file.dir) {
            context.addFile( new File(
                filename,
                await zip.file(`${path}/${filename}`).async('blob'),
                context
            ));
        }
    }));
    return directory;
}

export async function loadConfiguration( url = '/assets/project/config.json' ) {
    const response = await fetch(url);
    const data = await response.json();
    return data;
}

async function loadZip( url = '/assets/project/filesystem.zip' ) {
    const response = await fetch(url);
    const data = await response.blob();
    return JSZip.loadAsync(data);
}
