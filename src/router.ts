import Vue from 'vue';
import Router from 'vue-router';
import Login from './views/Login.vue';
import System from './views/System.vue';
import Backend from './views/Backend.vue';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      redirect: '/login',
      meta: {
        title: 'Laptop - Anmeldung'
      },
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      meta: {
        title: 'Laptop - Anmeldung'
      }
    },
    {
      path: '/system',
      name: 'system',
      component: System,
      meta: {
        title: 'Laptop - System'
      },
    },
    {
      path: '/backend',
      name: 'backend',
      component: Backend,
      meta: {
        title: 'Einstellungen für den Laptop'
      },
    },
  ],
});

router.beforeEach( (to, from, next) => {
  const title = to.matched.slice().reverse().find( r => r.meta.title );
  if (title) {
    document.title = title.meta.title;
  }
  next();
});

export default router;