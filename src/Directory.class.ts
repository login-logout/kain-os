import File from './File.interface';

export default class Directory {
    constructor( public name: string,
                 public parent?: Directory,
                 public directories: Directory[] = [],
                 public files: File[] = [] ) {
    }

    createDirectory( path: string ) {
        // remove forwarding and trailing slash
        if (path.charAt(0) === '/') {
            path = path.slice(1);
        }
        if (path.charAt(path.length - 1) === '/') {
            path = path.slice(-1);
        }
        const route = path.split('/');
        return route.reduce( (ctx, dirname) => {
            return ctx.getDirectory(dirname) || ctx.addDirectory( new Directory(dirname) );
        }, this as Directory);
    }

    addFile( file: File ) {
        this.files.push(file);
    }

    addDirectory( directory: Directory ) {
        directory.parent = this;
        this.directories.push(directory);
        return directory;
    }

    getDirectory( dirname: string ) {
        return this.directories.find( directory => directory.name === dirname );
    }

    getFullPath(): string {
        if (this.parent) {
            return this.parent.getFullPath() + '/' + this.name;
        } else {
            return '';
        }
    }
}