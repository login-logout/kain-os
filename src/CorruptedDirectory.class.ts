import Directory from './Directory.class';
import File from './File.interface';

export class CorruptedDirectory extends Directory {
    constructor( name: string,
                 public corruptedSubdirectoryName: string,
                 parent?: Directory,
                 directories?: Directory[],
                 files?: File[] ) {
        super( name, parent, directories, files );
    }

    static fromDirectory( directory: Directory, corruptedSubdirectoryName: string ) {
        return new CorruptedDirectory( directory.name, corruptedSubdirectoryName, directory.parent, directory.directories, directory.files );
    }
}