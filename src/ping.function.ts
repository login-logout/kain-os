

export default function ping() {
    const audio = new Audio();
    audio.src = '/assets/project/ping.wav';
    audio.play();
}